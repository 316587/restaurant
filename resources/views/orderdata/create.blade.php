@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3>test</h3>
      </div>
    </div>

    @if ($errors->any())
      <div class="alert alert-danger">
        <strong>Whoops! </strong> there where some problems with your input.<br>
        <ul>
          @foreach ($errors as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form action="{{route('orderdata.store')}}" method="post">
      @csrf
      <div class="row">
        <div class="col-md-12">
          <strong>Datum en tijd :</strong>
          <input type="datetime-local" name="datetime" class="form-control" placeholder="vul hier je datum en tijd in">
        </div>
        <div class="col-md-12">
          <strong>totaal_prijs :</strong>
          <input type="number" name="total_price" class="form-control" placeholder="totaal_prijs">
        </div>

        <div class="col-md-12">
          <a href="{{route('orderdata.index')}}" class="btn btn-sm btn-success">Back</a>
          <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </div>
      </div>
    </form>

  </div>
@endsection

