@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>test</h3>
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <strong>Datum en tijd : </strong> {{$orderdata->datetime}}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <strong> totaal_prijs: </strong> {{$orderdata->total_price}}
        </div>
      </div>
      <div class="col-md-12">
        <a href="{{route('orderdata.index')}}" class="btn btn-sm btn-success">Back</a>
      </div>
    </div>
  </div>
@endsection