<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orderdata;

class OrderdataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orderdata::latest()->paginate(5);
        return view('orderdata.index', compact('orders'))
                  ->with('i', (request()->input('page',1) -1)*5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Orderdata.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'title'       => 'required'
        ]);
        Orderdata::create($request->all());
        return redirect()->route('orderdata.index')
                        ->with('success', 'new order created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderdata= Orderdata::find($id);
        return view('orderdata.detail', compact('orderdata'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderdata = Orderdata::find($id);
        return view('orderdata.edit', compact('orderdata'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'datetime' => 'required',
        'total_price' => 'required'
      ]);
      $orderdata = Orderdata::find($id);
      $orderdata->datetime = $request->get('datetime');
      $orderdata->total_price = $request->get('total_price');
      $orderdata->save();
      return redirect()->route('orderdata.index')
                      ->with('success', 'Order  updated successfully');
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orderdata = Orderdata::find($id);
        $orderdata->delete();
        return redirect()->route('orderdata.index')
                        ->with('success', 'order deleted successfully');
    }
}