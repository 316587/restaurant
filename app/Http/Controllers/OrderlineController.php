<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orderline;

class OrderlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderlines = Orderline::latest()->paginate(5);
        return view('orderline.index', compact('orderline'))
                  ->with('i', (request()->input('page',1) -1)*5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Orderline.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
          'Order_id'    => 'required',
          'description' => 'required',
          'amount'      => 'required',
          'price'       => 'required'
        ]);
        Orderline::create($request->all());
        return redirect()->route('orderline.index')
                        ->with('success', 'new order created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderline= Orderline::find($id);
        return view('orderline.detail', compact('orderline'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderline = Orderline::find($id);
        return view('orderline.edit', compact('orderline'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'Order_id'    => 'required',
        'description' => 'required',
        'amount'      => 'required',
        'price'       => 'required'
      ]);
      $orderline = Orderline::find($id);
      $orderline->Order_id      = $request->get('Order_id');
      $orderline->description   = $request->get('description');
      $orderline->amount        = $request->get('amount');
      $orderline->price         = $request->get('price');
      $orderline->save();
      return redirect()->route('orderline.index')
                      ->with('success', 'Order  updated successfully');
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orderline = Orderline::find($id);
        $orderline->delete();
        return redirect()->route('orderline.index')
                        ->with('success', 'order deleted successfully');
    }
}