<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordertype extends Model
{
    public function Orderdata() 
    {
        return $this->belongsToMany('App/Orderdata');
    }
    
    protected $fillable = [
        'title',
        ];
    }
