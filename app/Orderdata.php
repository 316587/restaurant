<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Orderdata extends Model{
    public function User()
    
    {
        return $this->belongsToMany('App\User');
    }
    
    public function Order_Type()

    {
        return $this->belongsToMany('App\Order_Type');
    }

    public function Order_Line()

    {
        return $this->belongsToMany('App\Order_Line');
    }


    protected $fillable = [
        'User_id','Type_id','Manage_id','datetime', 'total_price',
        ];
    }
