<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function User_Role() 
    {
        return $this->hasMany('App/User_Role');
    }
}
